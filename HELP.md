
Swagger:
http://localhost:8080/stockItemService/swagger-ui/index.html


nächste Schritte:

	- Micrometer und Prometheus im Inventory-Service
	- Spring Boot Management (Actuator)
	  - liveness probe
	  - readiness probe
	- k8s
	- Grafana

---
Docker-Registry lokal per Docker aufsetzen:
	
	https://docs.docker.com/registry/deploying/
	
	docker run -d -p 5000:5000 --restart=always --name registry registry:2
	
	see https://github.com/distribution/distribution/blob/main/docs/spec/api.md

	http://localhost:5000/v2/_catalog
	http://localhost:5000/v2/<name>/tags/list
	

	docker run -d -p 8080:8080 -e SPRING_PROFILES_ACTIVE=local --restart=always --name inventory-service localhost:5000/inventory-service:0.1