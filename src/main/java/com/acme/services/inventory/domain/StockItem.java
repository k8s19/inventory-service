package com.acme.services.inventory.domain;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StockItem {

	private StockItemReference stockItemReference;
	
	@NonNull
	private String name;
	
	@NonNull
	private String description;
	
	@NonNull
	private BigDecimal price;
	
	@NonNull
	private Integer amount;
	
	@NonNull
	private Integer category;
	
}
