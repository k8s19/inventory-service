package com.acme.services.inventory.domain;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
@AllArgsConstructor
public class StockItemMetadata {

	@NonNull
	private Long referenceNumber;
	
	private LocalDateTime creationDateTime;
	
	private LocalDateTime modificationDateTime;
	
}
