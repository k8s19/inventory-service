package com.acme.services.inventory.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
@Data
public class StockItemReference {

	@NonNull
	private final Long value;

	@JsonCreator
	public static StockItemReference create(@JsonProperty("value") Long value) {
		return StockItemReference.of(value);
	}
}
