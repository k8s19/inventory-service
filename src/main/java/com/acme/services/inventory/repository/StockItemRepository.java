package com.acme.services.inventory.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.acme.services.inventory.repository.model.StockItemEntity;

/**
 * see
 * https://docs.spring.io/spring-data/commons/docs/current/reference/html/#repositories.query-methods.details
 * <br/>
 * 
 * see also
 * https://github.com/spring-projects/spring-data-examples/tree/master/jpa/jpa21
 * 
 */
public interface StockItemRepository extends JpaRepository<StockItemEntity, Long> {

}
