package com.acme.services.inventory.repository.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name =  "stock_item")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StockItemEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REFERENCE_NUMBER")
	private Long referenceNumber;

	@NotNull
	@Column(name = "name", unique = true, nullable = false)
	private String name;
	
	@NotNull
	@Column(name = "description", nullable = false)
	private String description;
	
	@NotNull
	@Column(name = "price", nullable = false, precision = 2)
	private BigDecimal price;
	
	@Column(name = "amount", nullable = false)
	private int amount;
	
	@Column(name = "category", nullable = false)
	@NotNull
	private Integer category;
	
	@Column(name = "creation_datetime", columnDefinition = "TIMESTAMP")
	private LocalDateTime creationDateTime;
	
	@Column(name = "modification_datetime", columnDefinition = "TIMESTAMP")
	private LocalDateTime modificationDateTime;
	
	@PreUpdate
	public void preUpdate() {
		modificationDateTime = LocalDateTime.now();
	}
	
	@PrePersist
	public void prePersist() {
		creationDateTime = LocalDateTime.now();
	}
	
}
