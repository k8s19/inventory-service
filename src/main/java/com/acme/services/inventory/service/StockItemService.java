package com.acme.services.inventory.service;

import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.acme.services.inventory.domain.StockItem;
import com.acme.services.inventory.domain.StockItemMetadata;
import com.acme.services.inventory.domain.StockItemReference;
import com.acme.services.inventory.repository.StockItemRepository;
import com.acme.services.inventory.repository.model.StockItemEntity;
import com.acme.services.inventory.service.mapper.StockItemMapper;
import com.acme.services.inventory.web.exception.NotFoundException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
@Validated
public class StockItemService {

	private final StockItemRepository stockItemRepository;

	private final StockItemMapper stockItemMapper;

	@Transactional(propagation = Propagation.REQUIRED)
	public StockItemReference saveOrUpdate(@NotNull @Valid StockItem stockItem) {

		log.info("updating or creating stock item with {}, name {}", stockItem.getStockItemReference(), stockItem.getName());

		Optional<StockItemEntity> optPersistentEntity = Optional.ofNullable(stockItem.getStockItemReference())
				.map(StockItemReference::getValue)
				.map(stockItemRepository::findById)
				.flatMap(stockItemEntity -> stockItemEntity);

		StockItemEntity stockItemEntity = optPersistentEntity.map(entity -> updateStockItemEntity(stockItem, entity))
				.orElseGet(() -> saveStockItem(stockItem));

		return StockItemReference.of(stockItemEntity.getReferenceNumber());
	}

	public Optional<StockItem> loadStockItem(@NotNull StockItemReference stockItemReference) {
		return stockItemRepository.findById(stockItemReference.getValue())
			.map(stockItemMapper::toStockItem);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteStockItem(@NotNull StockItemReference stockItemReference) {
		
		stockItemRepository.findById(stockItemReference.getValue()).ifPresentOrElse(
				stockItemEntity -> {
					stockItemRepository.delete(stockItemEntity);
				},
				() -> {
					throw new NotFoundException(stockItemReference.getValue().toString(),
							String.format("Stock item '%s' not found"));
				}
		);
	}

	public Optional<StockItemMetadata> loadMetadata(@NotNull StockItemReference stockItemReference) {
		return stockItemRepository.findById(stockItemReference.getValue())
				.map(stockItemMapper::toStockItemMetadata);
	}

	private StockItemEntity updateStockItemEntity(StockItem stockItem, StockItemEntity stockItemEntity) {
		StockItemMapper.updateStockItemEntity(stockItem, stockItemEntity);
		return stockItemEntity;
	}

	private StockItemEntity saveStockItem(StockItem stockItem) {
		StockItemEntity stockItemEntity = stockItemMapper.toStockItemEntity(stockItem);
		stockItemRepository.save(stockItemEntity);
		return stockItemEntity;
	}

}
