package com.acme.services.inventory.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import com.acme.services.inventory.domain.StockItem;
import com.acme.services.inventory.domain.StockItemMetadata;
import com.acme.services.inventory.domain.StockItemReference;
import com.acme.services.inventory.repository.model.StockItemEntity;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface StockItemMapper {

	@Mapping(target = "creationDateTime", ignore = true)
	@Mapping(target = "modificationDateTime", ignore = true)
	@Mapping(source = "stockItemReference.value", target = "referenceNumber")
	StockItemEntity toStockItemEntity(StockItem stockItem);

	@Mapping(source = "referenceNumber", target = "stockItemReference", qualifiedByName = "toStockItemReference")
	StockItem toStockItem(StockItemEntity stockItemEntity);
	
	StockItemMetadata toStockItemMetadata(StockItemEntity stockItemEntity);

	@Named("toStockItemReference")
	default StockItemReference toStockItemReference(Long value) {
		return StockItemReference.of(value);
	}

	static void updateStockItemEntity(StockItem stockItem, StockItemEntity stockItemEntity) {
		stockItemEntity.setAmount(stockItem.getAmount());
		stockItemEntity.setCategory(stockItem.getCategory());
		stockItemEntity.setDescription(stockItem.getDescription());
		stockItemEntity.setName(stockItem.getName());
		stockItemEntity.setPrice(stockItem.getPrice());
	}

}
