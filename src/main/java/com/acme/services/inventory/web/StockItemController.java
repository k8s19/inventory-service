package com.acme.services.inventory.web;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.acme.services.inventory.domain.StockItem;
import com.acme.services.inventory.domain.StockItemMetadata;
import com.acme.services.inventory.domain.StockItemReference;
import com.acme.services.inventory.service.StockItemService;
import com.acme.services.inventory.web.exception.NotFoundException;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Validated
public class StockItemController {

	private final StockItemService stockItemService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StockItemReference> createStockItem(@NotNull @Valid @RequestBody StockItem stockItem) {
		
		if (stockItem.getStockItemReference() != null) {
			throw new IllegalArgumentException("New stock item must not have a reference number");
		}
		
		StockItemReference stockItemReference = stockItemService.saveOrUpdate(stockItem);
		
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.body(stockItemReference);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StockItemReference> updateStockItem(@NotNull @Valid @RequestBody StockItem stockItem) {
		
		Objects.requireNonNull(stockItem.getStockItemReference());
		
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.body(stockItemService.saveOrUpdate(stockItem));
	}

	@GetMapping(path = "/{stock_item_reference}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StockItem> loadStockItem(@NotNull @PathVariable("stock_item_reference") Long stockItemReference) {
		
		StockItem stockItem = 
				stockItemService.loadStockItem(StockItemReference.of(stockItemReference))
				.orElseThrow(
						() -> new NotFoundException(String.valueOf(stockItemReference), "Stock item not found"));
		
		return ResponseEntity.ok(stockItem);
	}

	@DeleteMapping(path = "/{stock_item_reference}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> deleteStockItem(@NotNull @PathVariable("stock_item_reference") Long stockItemReference) {
		stockItemService.deleteStockItem(StockItemReference.of(stockItemReference));
		return ResponseEntity.noContent().build();
	}

	@GetMapping(path = "/{stock_item_reference}/metadata", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StockItemMetadata> loadMetadata(
			@NotNull @PathVariable("stock_item_reference") Long stockItemReference) {
		
		StockItemMetadata stockItemMetadata = 
				stockItemService.loadMetadata(StockItemReference.of(stockItemReference))
				.orElseThrow(
						() -> new NotFoundException(String.valueOf(stockItemReference), "Stock item not found"));
		
		return ResponseEntity.ok(stockItemMetadata);
	}
}
