package com.acme.services.inventory.web.exception;

import org.springframework.http.MediaType;

public abstract class AbstractProblemException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public static final String JSON_PROBLEM_MEDIA_TYPE = MediaType.APPLICATION_PROBLEM_JSON_VALUE;

  /** Constructs a new exception with {@code null} as its detail message. */
  public AbstractProblemException() {
    super();
  }

  /**
   * call to {@link #initCause}.
   *
   * @param message the detail message. The detail message is saved for later retrieval by the
   *     {@link #getMessage()} method.
   */
  public AbstractProblemException(String message) {
    super(message);
  }
}
