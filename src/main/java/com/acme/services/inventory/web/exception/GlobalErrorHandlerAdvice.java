package com.acme.services.inventory.web.exception;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.acme.services.inventory.web.exception.problem.Problem;
import com.acme.services.inventory.web.exception.problem.ValidationProblem;
import com.acme.services.inventory.web.exception.problem.Violation;

/**
 * @see https://www.baeldung.com/global-error-handler-in-a-spring-rest-api <br>
 * 
 *      An important thing to note here is that using @Valid annotation results
 *      in MethodArgumentNotValidException being thrown when validation fails,
 *      but @Validated results in ConstraintViolationException being thrown.
 *      Since these exceptions have different ways of abstracting the error
 *      messages associated with validation, it is important to have different
 *      error handlers for both of these.
 *
 *      <pre>
 * 	- @javax.validation.Valid ->  {@link org.springframework.web.bind.MethodArgumentNotValidException}
 * 	- @org.springframework.validation.annotation.Validated ->  {@link javax.validation.ConstraintViolationException}
 * 		</pre>
 *
 *      This class is handling the following exceptions (with appertaining
 *      problem):
 *
 *      <pre>
 *  - {@link RuntimeException} -> {@link Problem}
 *  - {@link ProblemException} -> {@link Problem}
 *  - {@link ValidationProblemException} -> {@link ValidationProblem}
 *  - {@link ResourceNotFoundException} -> {@link Problem}
 *  - {@link MethodArgumentNotValidException} -> {@link ValidationProblem}
 *  - {@link ConstraintViolationException} -> {@link ValidationProblem}
 *  - {@link BindException} -> {@link ValidationProblem}
 * 		</pre>
 */
@ControllerAdvice
public class GlobalErrorHandlerAdvice {

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalErrorHandlerAdvice.class);

	private static final String DEFAULT_ERROR_MSG = "something went wrong";

	@ResponseBody
	@ExceptionHandler({ Exception.class, RuntimeException.class })
	public ResponseEntity<Problem> onException(Exception e, HttpServletRequest httpServletRequest) {

		LOGGER.error("Caught unhandled exception while handling a request", e);

		String detail = DEFAULT_ERROR_MSG;
		Integer status = HttpStatus.INTERNAL_SERVER_ERROR.value();
		String title = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
				.body(makeProblem(detail, status, title, httpServletRequest));
	}

	/*
	 * @ExceptionHandler(ResourceNotFoundException.class)
	 * 
	 * @ResponseStatus(HttpStatus.NOT_FOUND)
	 * 
	 * @ResponseBody public ResponseEntity<Problem> onException(
	 * ResourceNotFoundException e, HttpServletRequest httpServletRequest) {
	 * 
	 * log.debug("Caught runtime/system exception while handling a request", e);
	 * 
	 * String detail = e.getMessage(); Integer status =
	 * HttpStatus.NOT_FOUND.value(); String title =
	 * HttpStatus.NOT_FOUND.getReasonPhrase();
	 * 
	 * return ResponseEntity.status(HttpStatus.NOT_FOUND)
	 * .contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
	 * .body(makeProblem(detail, status, title, httpServletRequest)); }
	 */
	@ExceptionHandler(ProblemException.class)
	@ResponseBody
	public ResponseEntity<Problem> onException(ProblemException pe) {

		LOGGER.debug("Caught ProblemException while handling a request", pe);

		return ResponseEntity.status(pe.getProblem().getStatus())
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE)).body(pe.getProblem());
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public ResponseEntity<Problem> onException(IllegalArgumentException e, HttpServletRequest httpServletRequest) {

		LOGGER.error("Caught unhandled exception while handling a request", e);

		String detail = e.getMessage();
		Integer status = HttpStatus.BAD_REQUEST.value();
		String title = HttpStatus.BAD_REQUEST.getReasonPhrase();

		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
				.body(makeProblem(detail, status, title, httpServletRequest));
	}

	@ExceptionHandler(ValidationProblemException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ResponseBody
	public ResponseEntity<ValidationProblem> onException(ValidationProblemException vpe,
			HttpServletRequest httpServletRequest) {

		// why error level? because validation errors in service-to-service
		// communication mean that
		// there is a bug
		LOGGER.error("Caught ViolationProblemException while handling a request", vpe);

		vpe.getProblem().setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
		vpe.getProblem().setInstance(httpServletRequest.getRequestURI());

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE)).body(vpe.getProblem());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ResponseEntity<ValidationProblem> handle(MethodArgumentNotValidException exception,
			HttpServletRequest httpServletRequest) {

		String title = HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase();
		Integer status = HttpStatus.UNPROCESSABLE_ENTITY.value();

		List<Violation> violations = exception.getBindingResult().getFieldErrors().stream().distinct()
				.map(this::makeViolation).collect(Collectors.toList());

		LOGGER.debug("Invalid request - method argument validation failed: ", violations);

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE)).body(makeViolationProblem(
						"Method argument validation failed", status, title, violations, httpServletRequest));
	}

	@ExceptionHandler(BindException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ResponseEntity<ValidationProblem> handle(BindException e, HttpServletRequest httpServletRequest) {
		String title = HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase();
		Integer status = HttpStatus.UNPROCESSABLE_ENTITY.value();

		List<Violation> violations = e.getBindingResult().getFieldErrors().stream().distinct().map(this::makeViolation)
				.collect(Collectors.toList());

		LOGGER.debug("Invalid request - bind failed: ", violations);

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
				.body(makeViolationProblem("Bind failed", status, title, violations, httpServletRequest));
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ResponseEntity<Problem> handle(ConstraintViolationException exception,
			HttpServletRequest httpServletRequest) {

		String title = HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase();
		Integer status = HttpStatus.UNPROCESSABLE_ENTITY.value();

		List<Violation> violations = exception.getConstraintViolations().stream().map(this::makeViolation)
				.collect(Collectors.toList());

		LOGGER.debug("Invalid request - validation failed: ", violations);

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
				.body(makeViolationProblem("Validation failed", status, title, violations, httpServletRequest));
	}

	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<ValidationProblem> handle(MissingServletRequestParameterException exception,
			HttpServletRequest httpServletRequest) {

		LOGGER.error("Invalid request! Request parameter is missing!" + exception.getMessage());

		String title = HttpStatus.BAD_REQUEST.getReasonPhrase();
		Integer status = HttpStatus.BAD_REQUEST.value();
		String detail = exception.getMessage();

		Violation violation = Violation.builder()
				.field(exception.getParameterName())
				.message(detail)
				.build();

		List<Violation> violations = Arrays.asList(violation);

		return ResponseEntity.badRequest().contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
				.body(makeViolationProblem(detail, status, title, violations, httpServletRequest));
	}

	@ExceptionHandler(MissingPathVariableException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<ValidationProblem> handle(MissingPathVariableException exception,
			HttpServletRequest httpServletRequest) {

		LOGGER.error("Invalid request! Path variable is missing!" + exception.getMessage());

		String title = HttpStatus.BAD_REQUEST.getReasonPhrase();
		Integer status = HttpStatus.BAD_REQUEST.value();
		String detail = exception.getMessage();

		Violation violation = Violation.builder()
			.field(exception.getVariableName())
			.message(detail)
			.build();

		List<Violation> violations = Arrays.asList(violation);

		return ResponseEntity.badRequest().contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
				.body(makeViolationProblem(detail, status, title, violations, httpServletRequest));
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public ResponseEntity<Problem> handle(HttpRequestMethodNotSupportedException exception,
			HttpServletRequest httpServletRequest) {

		LOGGER.error("Invalid request - " + exception.getMessage());

		String title = HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase();
		Integer status = HttpStatus.METHOD_NOT_ALLOWED.value();
		String detail = exception.getMessage();

		return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
				.body(makeProblem(detail, status, title, httpServletRequest));
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseBody
	public ResponseEntity<Problem> conflict(NotFoundException exception, HttpServletRequest httpServletRequest) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

		String title = HttpStatus.NOT_FOUND.getReasonPhrase();
		Integer status = HttpStatus.NOT_FOUND.value();
		String detail = exception.getMessage();

		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.contentType(MediaType.valueOf(ProblemException.JSON_PROBLEM_MEDIA_TYPE))
				.body(makeProblem(detail, status, title, httpServletRequest));
	}

	// --- helper ---

	private ValidationProblem makeViolationProblem(String detail, Integer status, String title,
			List<Violation> violations, HttpServletRequest httpServletRequest) {
		return ValidationProblem.builder()
				.detail(detail)
				.status(status)
				.title(title)
				.instance(httpServletRequest.getRequestURI())
				.violations(Optional.ofNullable(violations).orElseGet(Collections::emptyList))
				.build();
	}

	private Problem makeProblem(String detail, Integer status, String title, HttpServletRequest httpServletRequest) {
		return Problem.builder()
				.detail(detail)
				.status(status)
				.title(title)
				.instance(httpServletRequest.getRequestURI())
				.build();
	}

	private Violation makeViolation(FieldError fe) {
		return Violation.builder()
				.field(fe.getObjectName() + "." + fe.getField())
				.message(fe.getDefaultMessage())
				.build();
	}

	private Violation makeViolation(ConstraintViolation<?> constraintViolation) {
		return Violation.builder()
				.field(constraintViolation.getPropertyPath().toString())
				.message(constraintViolation.getMessage())
				.build();
	}
}
