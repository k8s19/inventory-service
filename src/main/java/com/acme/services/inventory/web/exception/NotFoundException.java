package com.acme.services.inventory.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This is not a {@link ProblemException}!
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Requested item not found")
public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String resource;
	
	public <T> NotFoundException(String resource, String message) {
		super(message);
		this.resource = resource;
	}

	public String getResource() {
		return resource;
	}
}
