package com.acme.services.inventory.web.exception;

import javax.validation.constraints.NotNull;

import com.acme.services.inventory.web.exception.problem.Problem;

public class ProblemException extends AbstractProblemException {

	private static final long serialVersionUID = 1L;

	private final Problem problem;

	public ProblemException(@NotNull Problem problem) {
		super(problem.toString());
		this.problem = problem;
	}

	public Problem getProblem() {
		return problem;
	}

}
