package com.acme.services.inventory.web.exception;

/**
 * This class is used when it is assumed that the server error response should
 * have a {@link Problem} JSON but the attempt to deserialize the body has
 * failed. In this case either the Content-Type header was mistakenly set to
 * {@see ProblemException#JSON_PROBLEM_MEDIA_TYPE} or the JSON belongs to an
 * unknown {@link Problem} subclass.
 *
 * <p>
 * This is a client side exception!!
 */
public class UnknownProblemException extends AbstractProblemException {

	/** */
	private static final long serialVersionUID = 1L;

	private final String responseBody;

	private final int httpStatusCode;

	private final String path;

	public UnknownProblemException(String message, String responseBody, int httpStatusCode, String path) {
		super(message);
		this.responseBody = responseBody;
		this.httpStatusCode = httpStatusCode;
		this.path = path;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	public String getPath() {
		return path;
	}

}
