package com.acme.services.inventory.web.exception;

import javax.validation.constraints.NotNull;

import com.acme.services.inventory.web.exception.problem.ValidationProblem;

public class ValidationProblemException extends AbstractProblemException {

	private static final long serialVersionUID = 1L;

	private final ValidationProblem problem;

	public ValidationProblemException(@NotNull ValidationProblem violationProblem) {
		super(violationProblem.toString());
		this.problem = violationProblem;
	}

	public ValidationProblem getProblem() {
		return problem;
	}

}
