package com.acme.services.inventory.web.exception.problem;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Validated
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({ @JsonSubTypes.Type(value = ValidationProblem.class, name = "ValidationProblem"), })
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Problem {

	@JsonProperty("type")
	@Builder.Default
	private String type = "about:blank";

	@JsonProperty("title")
	private String title;

	@Min(100)
	@Max(600)
	@JsonProperty("status")
	private Integer status;

	@JsonProperty("detail")
	private String detail;

	@JsonProperty("instance")
	private String instance;

}
