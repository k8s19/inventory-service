package com.acme.services.inventory.web.exception.problem;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Violation
 */
@Validated
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Violation {

	@JsonProperty("field")
	private String field;

	@JsonProperty("message")
	private String message;

}
