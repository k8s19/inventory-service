package com.acme.services.inventory;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(InventoryServiceApplication.class)
public class IntegrationTestConfig {

	// add mock beans here (@MockBean)
	
}
