package com.acme.services.inventory;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.acme.services.inventory.domain.StockItem;
import com.acme.services.inventory.domain.StockItemReference;
import com.acme.services.inventory.repository.StockItemRepository;
import com.acme.services.inventory.repository.model.StockItemEntity;
import com.acme.services.inventory.web.StockItemController;

/**
 * This class uses testcontainer and hence runs with an mysql database.
 */
@IntegrationTestSetup
class RoundTripIT {

	@Autowired
	private StockItemController stockItemController;
	
	@Autowired
	private StockItemRepository stockItemRepository;
	
	@Test	
	void shouldSaveNewStockItem() {
		// given
		StockItem givenStockItem = givenStockItem("Nike Sneaker", "best sneakers ever");
		
		// when
		ResponseEntity<StockItemReference> stockItemRefEntity = stockItemController.createStockItem(givenStockItem);
		
		// then
		assertThat(stockItemRefEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		Long referenceNumber = stockItemRefEntity.getBody().getValue();
		assertThat(referenceNumber).isGreaterThan(0);
		
		Optional<StockItemEntity> optStockItem = stockItemRepository.findById(referenceNumber);
		assertThat(optStockItem).isPresent().get().satisfies(stockItemEntity -> {
			assertThat(stockItemEntity.getName()).isEqualTo(givenStockItem.getName());
			assertThat(stockItemEntity.getCategory()).isEqualTo(givenStockItem.getCategory());
			assertThat(stockItemEntity.getDescription()).isEqualTo(givenStockItem.getDescription());
			assertThat(stockItemEntity.getPrice()).isEqualTo(givenStockItem.getPrice());
			assertThat(stockItemEntity.getReferenceNumber()).isEqualTo(referenceNumber);
		});
	}

	private StockItem givenStockItem(String name, String description) {
		return StockItem.builder()
			.category(216)
			.description(description)
			.name(name)
			.amount(200)
			.price(BigDecimal.valueOf(99))
			.build();
	}
}
