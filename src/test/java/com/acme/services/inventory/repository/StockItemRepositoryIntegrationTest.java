package com.acme.services.inventory.repository;

import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;
import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import com.acme.services.inventory.repository.model.StockItemEntity;

import lombok.extern.slf4j.Slf4j;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Slf4j
public class StockItemRepositoryIntegrationTest {

	@Autowired
	private StockItemRepository stockItemRepository;
	
	@Transactional
	@Rollback
	@Test
	public void testSaveAndLoad() {
		log.info("testing save and load");
		
		// given
		StockItemEntity stockItemEntity = StockItemEntity.builder()
			.name("FFP2 Mask")
			.description("does not protect from virus")
			.price(BigDecimal.valueOf(19.99d))
			.amount(10_000)
			.category(42)
			.build();
		
		// when 
		stockItemRepository.save(stockItemEntity);
		
		// then
		assertThat(stockItemEntity.getReferenceNumber()).isNotNull();
		assertThat(stockItemEntity.getCreationDateTime()).isNotNull();
		assertThat(stockItemRepository.count()).isOne();
	}
	
	@Transactional
	@Rollback
	@Test
	public void testFailSaveDueToIncompleteData() {
		log.info("testing save and load");
		
		// given
		StockItemEntity stockItemEntity = StockItemEntity.builder()
			.name("FFP2 Mask")
			.description("does not protect from virus")
			.price(BigDecimal.valueOf(19.99d))
			.amount(10_000)
			.build();
		
		// when & then
		assertThatExceptionOfType(ConstraintViolationException.class)
				.isThrownBy(() -> stockItemRepository.save(stockItemEntity));
		
	}
	
	
}
