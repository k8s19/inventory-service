package com.acme.services.inventory.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.acme.services.inventory.domain.StockItem;
import com.acme.services.inventory.domain.StockItemReference;
import com.acme.services.inventory.repository.StockItemRepository;
import com.acme.services.inventory.repository.model.StockItemEntity;
import com.acme.services.inventory.service.mapper.StockItemMapper;
import com.acme.services.inventory.service.mapper.StockItemMapperImpl;

@ExtendWith(MockitoExtension.class)
public class StockItemServiceTest {
	
	@Mock
	private StockItemRepository stockItemRepository;

	@Spy
	private StockItemMapper stockItemMapper = new StockItemMapperImpl();
	
	@InjectMocks
	private StockItemService stockItemService;
	
	@Test
	public void saveOrUpdate() {
		
		// given
		StockItem stockItem = givenStockItem();
		StockItemEntity stockItemEntity = stockItemMapper.toStockItemEntity(stockItem);
		
		when(stockItemRepository.findById(eq(stockItem.getStockItemReference().getValue())))
			.thenReturn(Optional.of(stockItemEntity));
		
		// when
		StockItemReference stockItemReference = stockItemService.saveOrUpdate(stockItem);
		
		// then
		assertThat(stockItemReference).isNotNull();
		assertThat(stockItemReference.getValue()).isEqualTo(stockItem.getStockItemReference().getValue());
	}
	
	@Test
	public void loadStockItem() {
		
		// given
		StockItemReference stockItemReference = StockItemReference.of(123l);
		StockItemEntity stockItemEntity = givenStockItemEntity();
		when(stockItemRepository.findById(stockItemReference.getValue()))
			.thenReturn(Optional.of(stockItemEntity));
		
		// when
		Optional<StockItem> optStockItem = stockItemService.loadStockItem(stockItemReference);
		
		// then 
		assertThat(optStockItem).isPresent();
		assertThat(optStockItem).get().satisfies(stockItem -> {
			assertThat(stockItem.getName()).isEqualTo(stockItemEntity.getName());
			assertThat(stockItem.getCategory()).isEqualTo(stockItemEntity.getCategory());
			assertThat(stockItem.getDescription()).isEqualTo(stockItemEntity.getDescription());
			assertThat(stockItem.getPrice()).isEqualTo(stockItemEntity.getPrice());
			assertThat(stockItem.getStockItemReference().getValue()).isEqualTo(stockItemEntity.getReferenceNumber());
		});
	}
	
	private StockItem givenStockItem() {
		return StockItem.builder()
				.stockItemReference(StockItemReference.of(123l))
				.amount(100)
				.category(2)
				.description("Face mask FFP6")
				.name("FFP6 Mask")
				.price(BigDecimal.valueOf(42L))
				.build();
	}

	private StockItemEntity givenStockItemEntity() {
		return StockItemEntity.builder()
				.referenceNumber(216L)
				.name("Heineken")
				.description("Original Heineken")
				.price(BigDecimal.valueOf(2.99))
				.amount(66)
				.category(12121)
				.creationDateTime(LocalDateTime.ofInstant(Instant.now().minusSeconds(60), ZoneId.systemDefault()))
				.modificationDateTime(LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()))
				.build();
	}
	
}
