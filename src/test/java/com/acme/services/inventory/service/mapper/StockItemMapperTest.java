package com.acme.services.inventory.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.junit.jupiter.api.Test;

import com.acme.services.inventory.domain.StockItem;
import com.acme.services.inventory.domain.StockItemMetadata;
import com.acme.services.inventory.repository.model.StockItemEntity;

public class StockItemMapperTest {

	@Test
	public void toStockItemEntity() {

		// given
		StockItem stockItem = StockItem.builder()
				.amount(100)
				.category(2)
				.description("Face mask FFP6")
				.name("FFP6 Mask")
				.price(BigDecimal.valueOf(42L))
				.build();
		
		//when
		StockItemEntity stockItemEntity = new StockItemMapperImpl().toStockItemEntity(stockItem);
		
		//then
		assertThat(stockItemEntity.getReferenceNumber()).isNull();
		assertThat(stockItemEntity.getAmount()).isEqualTo(stockItem.getAmount());
		assertThat(stockItemEntity.getCategory()).isEqualTo(stockItem.getCategory());
		assertThat(stockItemEntity.getName()).isEqualTo(stockItem.getName());
		assertThat(stockItemEntity.getPrice()).isEqualTo(stockItem.getPrice());
	}
	
	@Test
	public void toStockItem() {
		
		// given
		StockItemEntity stockItemEntity = givenStockItemEntity();
		
		// when
		StockItem stockItem = new StockItemMapperImpl().toStockItem(stockItemEntity);
		
		// then
		assertThat(stockItem.getStockItemReference().getValue()).isEqualTo(stockItemEntity.getReferenceNumber());
		assertThat(stockItem.getName()).isEqualTo(stockItemEntity.getName());
		assertThat(stockItem.getDescription()).isEqualTo(stockItemEntity.getDescription());
		assertThat(stockItem.getPrice()).isEqualTo(stockItemEntity.getPrice());
		assertThat(stockItem.getAmount()).isEqualTo(stockItemEntity.getAmount());
		assertThat(stockItem.getCategory()).isEqualTo(stockItemEntity.getCategory());
	}
	
	@Test
	public void toStockStockItemMetadata() {
		
		// given
		StockItemEntity stockItemEntity = givenStockItemEntity();
		
		// when
		StockItemMetadata stockItemMetadata = new StockItemMapperImpl().toStockItemMetadata(stockItemEntity);
		
		// then
		assertThat(stockItemMetadata.getReferenceNumber()).isEqualTo(stockItemEntity.getReferenceNumber());
		assertThat(stockItemMetadata.getCreationDateTime()).isEqualTo(stockItemEntity.getCreationDateTime());
		assertThat(stockItemMetadata.getModificationDateTime()).isEqualTo(stockItemEntity.getModificationDateTime());
	}

	private StockItemEntity givenStockItemEntity() {
		return StockItemEntity.builder()
				.referenceNumber(216L)
				.name("Heineken")
				.description("Original Heineken")
				.price(BigDecimal.valueOf(2.99))
				.amount(66)
				.category(12121)
				.creationDateTime(LocalDateTime.ofInstant(Instant.now().minusSeconds(60), ZoneId.systemDefault()))
				.modificationDateTime(LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()))
				.build();
	}

}
