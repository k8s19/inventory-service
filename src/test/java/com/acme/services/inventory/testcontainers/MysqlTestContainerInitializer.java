package com.acme.services.inventory.testcontainers;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.support.GenericApplicationContext;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.utility.DockerImageName;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MysqlTestContainerInitializer implements ApplicationContextInitializer<GenericApplicationContext> {
	
	private static MySQLContainer<?> mysqlContainer;

	@Override
	public void initialize(GenericApplicationContext applicationContext) {
		
		if (mysqlContainer != null && mysqlContainer.isRunning()) {
			throw new IllegalStateException(
					"Mysql container must not be running when a new spring context for integration tests is initialized");
		}
		
		log.info("starting mysql container for integration tests");
		
		DockerImageName mysqlDockerImageName = DockerImageName
				.parse("mysql:8.0.23")
				.asCompatibleSubstituteFor("mysql");

		mysqlContainer = new MySQLContainer<>(mysqlDockerImageName)
				.withDatabaseName("acme")
				.withUsername("inventory_user")
				.withPassword("secure.");
		
		mysqlContainer.start();
		
		TestPropertyValues properties = TestPropertyValues.of(
				String.format("spring.datasource.url=%s", mysqlContainer.getJdbcUrl()),
				String.format("spring.datasource.username=%s", mysqlContainer.getUsername()),
				String.format("spring.datasource.password=%s", mysqlContainer.getPassword()));
		
		properties.applyTo(applicationContext);
		
		applicationContext.addApplicationListener(applicationEvent -> {
			if (applicationEvent instanceof ContextClosedEvent) {
				if (mysqlContainer != null) {
					log.debug("processing ContextClosedEvent - stopping mysql container");
					mysqlContainer.stop();
					mysqlContainer = null;
				}
			}
		});

	}
}
