package com.acme.services.inventory.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.acme.services.inventory.domain.StockItem;
import com.acme.services.inventory.domain.StockItemMetadata;
import com.acme.services.inventory.domain.StockItemReference;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class StockItemControllerIT {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void saveItemAndLodMetadata() throws Exception {

		// 1. create a stock item
		StockItemReference stockItemReference = createStockItem("Nike_1", "Sneekers");
		
		// 2. load metadata
		// given 
		String path = "/" + stockItemReference.getValue() + "/metadata";
		HttpEntity<Void> metadataRequestEntity = new HttpEntity<>(givenHeaders());
		
		// when
		ResponseEntity<StockItemMetadata> getResponse = 
				restTemplate.exchange(
						path, 
						HttpMethod.GET, 
						metadataRequestEntity, 
						StockItemMetadata.class);
		// then 
		StockItemMetadata stockItemMetadata = getResponse.getBody();
		assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(stockItemMetadata).isNotNull();
		assertThat(stockItemMetadata.getReferenceNumber()).isEqualTo(stockItemReference.getValue());
		assertThat(stockItemMetadata.getCreationDateTime()).isNotNull();
		assertThat(stockItemMetadata.getModificationDateTime()).isNull();
	}
	
	@Test
	public void saveAndUpdateItem() throws Exception {

		// 1. create a stock item
		StockItemReference stockItemReference = createStockItem("Nike_2", "Shoes");
		
		// 2. update the stock item
		// given 
		StockItem updatedStockItem = givenStockItem(stockItemReference, "Nike_2", "Sneekers");
		HttpEntity<StockItem> updateHttpRequestEntity = new HttpEntity<StockItem>(updatedStockItem, givenHeaders());

		// when
		ResponseEntity<StockItemReference> updateResponse = 
				restTemplate.exchange(
						"/", 
						HttpMethod.PUT, 
						updateHttpRequestEntity, 
						StockItemReference.class);
		// then 
		StockItemReference updatedStockItemReference = updateResponse.getBody();
		assertThat(updatedStockItemReference.getValue()).isGreaterThanOrEqualTo(0);
		assertThat(updateResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);

		// 3. load the updated stock item
		// given 
		String path = "/" + updatedStockItemReference.getValue();
		HttpEntity<Void> getRequestEntity = new HttpEntity<>(givenHeaders());
		
		// when
		ResponseEntity<StockItem> getResponse = 
				restTemplate.exchange(
						path, 
						HttpMethod.GET, 
						getRequestEntity, 
						StockItem.class);
		// then 
		StockItem reloadedStockItem = getResponse.getBody();
		assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(reloadedStockItem).isNotNull();
		assertThat(reloadedStockItem.getStockItemReference().getValue()).isEqualTo(stockItemReference.getValue());
		assertThat(reloadedStockItem.getName()).isEqualTo(updatedStockItem.getName());
		assertThat(reloadedStockItem.getAmount()).isEqualTo(updatedStockItem.getAmount());
		assertThat(reloadedStockItem.getCategory()).isEqualTo(updatedStockItem.getCategory());
		assertThat(reloadedStockItem.getDescription()).isEqualTo(updatedStockItem.getDescription());
		assertThat(reloadedStockItem.getPrice().doubleValue()).isEqualTo(updatedStockItem.getPrice().doubleValue());
	}

	private StockItemReference createStockItem(String name, String description) {
		StockItem stockItem = givenStockItem(name, description);

		HttpHeaders headers = givenHeaders();
		HttpEntity<StockItem> httpRequestEntity = new HttpEntity<StockItem>(stockItem, headers);

		// when
		ResponseEntity<StockItemReference> createResponse = 
				restTemplate.exchange(
						"/", 
						HttpMethod.POST, 
						httpRequestEntity, 
						StockItemReference.class);
		
		// then 
		StockItemReference stockItemReference = createResponse.getBody();
		assertThat(createResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(stockItemReference.getValue()).isGreaterThanOrEqualTo(0);
		
		return stockItemReference;
	}

	private StockItem givenStockItem(String name, String description) {
		return givenStockItem(null, name, description);
	}

	private StockItem givenStockItem(StockItemReference stockItemReference, String name, String description) {
		return StockItem.builder()
			.category(216)
			.description(description)
			.name(name)
			.amount(200)
			.price(BigDecimal.valueOf(99))
			.stockItemReference(stockItemReference)
			.build();
	}

	private HttpHeaders givenHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		return headers;
	}

}
