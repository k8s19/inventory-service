package com.acme.services.inventory.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.acme.services.inventory.domain.StockItem;
import com.acme.services.inventory.domain.StockItemReference;
import com.acme.services.inventory.service.StockItemService;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = { StockItemController.class })
public class StockItemControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private StockItemService stockItemService;
	
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void saveOrUpdate() {
		System.out.println("saveOrUpdate");
	}

	@Test
	public void saveAndLodItem() throws Exception {

		// given 
		StockItem stockItem = givenStockItem("Shoes");
		when(stockItemService.saveOrUpdate(stockItem)).thenReturn(StockItemReference.of(1001L));
		
		// when
		ResultActions result = this.mockMvc.perform(post("/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json(stockItem)));
		
		// then
		String contentAsString = result.andExpect(status().isCreated())
				.andReturn()
				.getResponse()
				.getContentAsString();
		
		StockItemReference stockItemReference = readObject(StockItemReference.class, contentAsString);
		
		assertThat(stockItemReference).isNotNull();
		assertThat(stockItemReference.getValue()).isNotNull();
		
		// given 
		String path = "/" + stockItemReference.getValue();
		StockItem givenStockItem = givenStockItem(stockItemReference, "Shoes");
		when(stockItemService.loadStockItem(eq(stockItemReference))).thenReturn(Optional.of(givenStockItem));
		
		// when
		ResultActions resultActions = this.mockMvc.perform(get(path));
		
		String stockItemAsString = resultActions.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();

		// then
		StockItem reloadedStockItem = readObject(StockItem.class, stockItemAsString);
		assertThat(reloadedStockItem).isNotNull();
		assertThat(reloadedStockItem.getStockItemReference().getValue()).isEqualTo(stockItemReference.getValue());
		assertThat(reloadedStockItem.getName()).isEqualTo(stockItem.getName());
		assertThat(reloadedStockItem.getAmount()).isEqualTo(stockItem.getAmount());
		assertThat(reloadedStockItem.getCategory()).isEqualTo(stockItem.getCategory());
		assertThat(reloadedStockItem.getDescription()).isEqualTo(stockItem.getDescription());
		assertThat(reloadedStockItem.getPrice()).isEqualTo(stockItem.getPrice());

	}

	private StockItem givenStockItem(String description) {
		return givenStockItem(null, description);
	}

	private StockItem givenStockItem(StockItemReference stockItemReference, String description) {
		return StockItem.builder()
			.category(216)
			.description(description)
			.name("Nike")
			.amount(200)
			.price(BigDecimal.valueOf(99))
			.stockItemReference(stockItemReference)
			.build();
	}

	public String json(final Object object) throws IOException {
		return objectMapper.writeValueAsString(object);
	}

	public <T> T readObject(final Class<T> clazz, final String json) throws IOException {
		return objectMapper.readValue(json, clazz);
	}

}
